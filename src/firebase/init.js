import firebase from 'firebase'
import firestore from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBts3oZ_hDyR0Q-Co71jye-Djwmj5yZXPM",
  authDomain: "signin-app-e0bd0.firebaseapp.com",
  databaseURL: "https://signin-app-e0bd0.firebaseio.com",
  projectId: "signin-app-e0bd0",
  storageBucket: "signin-app-e0bd0.appspot.com",
  messagingSenderId: "627906261210",
  appId: "1:627906261210:web:44d047e71520b36f59b5e3",
  measurementId: "G-0HGM7R3VY1"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore().settings({ timestampsInSnapshots: true })

export default firebaseApp.firestore()
